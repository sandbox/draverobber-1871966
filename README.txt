Voting API Search Ranking (votingapi_search_ranking) module

SUMMARY

This module provides basic integration between Voting API and core Search
module by implementing hook_ranking(). Thus, it lets you promote nodes with
more or better votes in search results.

USAGE

Navigate to Configuration > Search and Metadata > Search settings, see 'Content
ranking' table at the bottom of the page.

You do not need to assign weights to all combinations of tag / value type;
instead, choose the value type most appropriate to voting modules being used,
e.g. 'average' for Fivestar or 'count' for Plus1.

KNOWN ISSUES

This module is a potential resource hog.
